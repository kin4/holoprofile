#

gitid=$( git -C ~/github.com/snippets rev-parse --short HEAD )
sed -i -e "s/jsversion: .*/jsversion: $gitid/" _config.yml
ls -1 mandalas > list.txt
perl bin/txt2json.pl -o list.json list.txt
jekyll build
sed -i -e "s/snippets@[^/]*/snippets@$gitid/" _site/index.html
rm _site/js/mandala.js
ln -s ../../js/mandala.js _site/js/mandala.js
rm _site/css/style.css
ln -s ../../css/style.css _site/css/style.css
