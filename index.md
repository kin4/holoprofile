---
layout: default
member: Michel G. Combes
aka: "penguin riding a bicycle"
home: https://qwant.com/?q=Michel+G.+Combes
aka-url: http://duckduckgo.com/?q=!g+%2B%22Michel+penguin+riding+a+bicycle%22
aka-url: http://duckduckgo.com/?q=!g+%2B%22Michel+penguin+à+vélo%22
---
![holosphere](img/holosphere.png)
## Profile of {{page.member}}

Polymathinc Engineer, blockchain architect,
4th Industrical ®Evolution Visionary,
GC-Bank™ founder,

I am seeking to transform the technology horizon so I can better
serve humanity while staying in harmony with one's environment.

Ledger Technology Evengelist, I promote [holoSphere][hs] and natural
evolution of blockchains such as blockRing™, based on Mychelium plateform.

[hs]: https://holosphere.gq

A Gradual Change Movement active member,
I propose alternatives to the system in place.

* voting plateform : Citizen initiative on the blockRing
* fair media : U-Nion™
* alternative banking: [http://gc-bank.org][gcb]
* tools developments to help advance humnaity

[gcb]: https://gc-bank.org/

Objective:
  to become _your next alternative banker_ for a true economic revolution

All made w/ ♡ from my kitchen

<style>
img[alt=holosphere] { float: right; border-radius: 50%; border: 1px dotted blue; }
</style>
<div id=mandalas style="display:none;"></div>



