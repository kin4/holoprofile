// Javascript "analog" clock

 var dat = new Date();
 var mstics = dat.getTime();
 const delta = -1584748800 * 1000
 var tics = Math.floor(mstics / 1000);
 console.log('tics:'+tics)
 // sideral day 86164.0905s
 // 12 hrs = 43200000 ms
 // 1t = 28 min = 1710997 ms
 // period : 43082045
// const period= 12 * 3600 * 1000
const period= 8765.81277 * 3600 / 365.25 / 2 * 1000
 const alpha = 2 * 30 - 45.0; // summer time + 2hrs
 var time = ((mstics + delta) % period) / 3600000;
 console.log('delta: '+delta)
 console.log('time: '+time)
 console.log('alpha: '+alpha)

 var angle = ((mstics + delta) % period) * 360 / period;
 var sidtime = angle / 360 * 12
 var sidmin = (sidtime % 1)  * 60
 var sidsec = (sidmin % 1) * 60
 console.log('angle: '+angle.toFixed(3))
 console.log('sidtime: '+sidtime)
 console.log('sidmin: '+sidmin)
 console.log('sidsec: '+sidsec)
 let hue =  angle - 60;

 var clock = document.getElementById('clock');
 if (clock) {
    let rotation = alpha + angle;
    clock.style.transform = 'rotate('+(rotation.toFixed(2))+'deg)';
    clock.style.filter = 'hue-rotate('+hue.toFixed(2)+'deg)';
    clock.style.backgroundColor = 'red';
    clock.style.borderRadius = '50%';
 }
