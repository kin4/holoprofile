#!/usr/bin/perl

# this script convert a plain text list into an json array
# input format : each line is a record
# item1
# item2
# ...

local *OUT;
my $target = '-';
if ($ARGV[0] eq '-o') {
 shift;
 $target = shift;
 open OUT,'>',$target or warn $!;
} else {
 *OUT = STDOUT;
}

# ----------------------------------
my @a;
while (<>) {
 chomp;
 push @a,$_;
}
# ----------------------------------
printf OUT qq'[%s]\n',join',',map { sprintf '"%s"',$_ } @a;
close OUT;
# ----------------------------------

exit $?;
1; # $Source: /my/perl/scripts/txt2json.pl$
